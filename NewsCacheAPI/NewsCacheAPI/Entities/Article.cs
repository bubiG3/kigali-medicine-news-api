﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewsCacheAPI.Entities
{
    public class Article
    {
        private static int idSeeder = 1;

        public Article()
        {
            Id = idSeeder++;
        }

        public int Id { get; set; }
        public Source Source { get; set; }
        public string Author { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public string UrlToImage { get; set; }
        public string PublishedAt { get; set; }
        public string Content { get; set; }
    }
}
