﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewsCacheAPI.Entities
{
    public class ResponseResult
    {
        public List<Article> Articles { get; set; }
    }
}
