﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NewsCacheAPI.Entities;
using NewsCacheAPI.Services;

namespace NewsCacheAPI.Controllers
{
    [Route("api/news")]
    [ApiController]
    public class NewsController: ControllerBase
    {
        private readonly IRepository _repository;

        public NewsController(IRepository repository)
        {
            _repository = repository;
        }

        // GET: api/news
        [HttpGet]
        public ActionResult<List<Article>> Get()
        {
            return _repository.GetAllArticles();
        }

        // GET: api/news/{Id}
        [HttpGet("{Id}")]
        public ActionResult<Article> Get(int id)
        {
            var article = _repository.GetArticleById(id);

            if(article == null)
            {
                return NotFound();
            }
            
            return article;
        }
    }
}
