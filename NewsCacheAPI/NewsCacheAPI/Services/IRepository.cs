﻿using NewsCacheAPI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewsCacheAPI.Services
{
    public interface IRepository
    {
        List<Article> GetAllArticles();
        Article GetArticleById(int id);
        void SetArticles(List<Article> articles);
    }
}
