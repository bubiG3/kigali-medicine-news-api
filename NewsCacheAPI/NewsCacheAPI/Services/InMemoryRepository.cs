﻿using NewsCacheAPI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewsCacheAPI.Services
{
    public class InMemoryRepository : IRepository
    {
        private List<Article> _articles;

        public InMemoryRepository()
        {
            // Test values for startup
            _articles = new List<Article>()
            {
                new Article(){
                    Source = new Source(){Id = "reuters", Name = "The Wall Street Journal"},
                    Author = "Rachel Wolfe",
                    Title = "Social Distancing Revives Old-Fashioned Romance: Long Talks, Serenades...",
                    Description = "Social Distancing Revives Old-Fashioned Romance: Long Talks, Serenades...\r\n\n \n \n \n (Second column, 12th story, link)\r\n\n \r\n\n \r\n\n \n Related stories:Every single worker has COVID at one farm on eve of harvest...\r\nCuomo Gives Businesses Power To Turn Away Custome…",
                    Url = "https://www.wsj.com/articles/social-distancing-revives-old-fashioned-romance-long-talks-serenades-11590776029",
                    UrlToImage = "https://images.wsj.net/im-190297/social",
                    PublishedAt = "2020-05-29T18:22:40Z",
                    Content = "Lora Pope has managed to find loveor, at least, likewithout leaving her childhood bedroom.\r\nWhen Ms. Pope, a 30-year-old travel blogger, returned from France to her familys Newfoundland, Canada, home… [+163 chars]"
                }
            };
        }

        public List<Article> GetAllArticles()
        {
            return _articles;
        }

        public Article GetArticleById(int id)
        {
            return _articles.FirstOrDefault(x => x.Id == id);
        }

        public void SetArticles(List<Article> articles)
        {
            _articles = articles;
        }
    }
}
