﻿using Microsoft.Extensions.Hosting;
using NewsCacheAPI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Policy;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace NewsCacheAPI.Services
{
    public class RequestService : IHostedService
    {
        private readonly IRepository _repository;
        private readonly IHttpClientFactory _clientFactory;
        private Timer timer;

        string url = "https://newsapi.org/v2/everything?q=kigali&apiKey=67cdb54db7fb4e50896c92751b644f78";

        public RequestService(IRepository repository, IHttpClientFactory clientFactory)
        {
            _repository = repository;
            _clientFactory = clientFactory;
        }

        // Executes on starting up the application
        public Task StartAsync(CancellationToken cancellationToken)
        {
            // Repeating every 20 min
            timer = new Timer(GetArticlesRequest, null, TimeSpan.Zero, TimeSpan.FromMinutes(20));
            return Task.CompletedTask;
        }

        // Executes on shutting the application down
        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        // Method for getting articles from the news api
        private async void GetArticlesRequest(object state)
        {
            ApiHelper.InitializeClient();
            using (HttpResponseMessage response = await ApiHelper.ApiClient.GetAsync(url))
            {
                if (response.IsSuccessStatusCode)
                {
                    ResponseResult articles = await response.Content.ReadAsAsync<ResponseResult>();
                    _repository.SetArticles(articles.Articles);
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }
    }
}
